variable "subnet_a_cidr" {
  description = "The CIDR block eg 10.0.1.0/24"
  default     = "10.0.1.0/24"
}

variable "subnet_b_cidr" {
  description = "The CIDR block eg 10.0.1.0/24"
  default     = "10.0.2.0/24"
}

variable "subnet_c_cidr" {
  description = "The CIDR block eg 10.0.1.0/24"
  default     = "10.0.3.0/24"
}