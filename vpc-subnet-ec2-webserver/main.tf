terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.59.0"
    }
  }
}

provider "aws" {
    region = "us-east-1"
    access_key = "redacted"
    secret_key = "redacted"
}

variable "subnet_prefix" {
  description = "The CIDR block eg 10.0.1.0/24"
  default = "10.0.2.0/24"
}

resource "aws_vpc" "my-vpc" {
  cidr_block = "10.0.0.0/16"
  tags = {
    "Name" = "my-vpc"
  }
}

resource "aws_internet_gateway" "my-igw" {
  vpc_id = aws_vpc.my-vpc.id

  tags = {
    Name = "my-igw"
  }
}

resource "aws_route_table" "my-route" {
  vpc_id = aws_vpc.my-vpc.id

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.my-igw.id
    }

  tags = {
    Name = "my-internet-traffic"
  }
}

resource "aws_route_table_association" "my-rta" {
  subnet_id = aws_subnet.my-public-subnet.id
  route_table_id = aws_route_table.my-route.id
}

resource "aws_subnet" "my-public-subnet" {
    vpc_id = aws_vpc.my-vpc.id
    cidr_block = var.subnet_prefix
    map_public_ip_on_launch = true

    tags = {
      "Name" = "my-public-subnet"
    }
}


resource "aws_network_interface" "eni" {
  subnet_id = aws_subnet.my-public-subnet.id
  private_ips = [ "10.0.2.15"]
  security_groups = [ aws_security_group.web-server-sg.id ]
}

resource "aws_eip" "one" {
  vpc                       = true
  network_interface         = aws_network_interface.eni.id
  associate_with_private_ip = "10.0.2.15"
  depends_on = [
    aws_internet_gateway.my-igw
  ]
}

resource "aws_security_group" "web-server-sg" {
  name        = "web-server-sg"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_vpc.my-vpc.id
  ingress {
      description      = "TLS for VPC"
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
      description      = "Port 80 for VPC"
      from_port        = 80
      to_port          = 80
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
      description      = "SSH for VPC"
      from_port        = 22
      to_port          = 22
      protocol         = "tcp"
      cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port = 0
    protocol = "-1"
    to_port = 0
  }

  tags = {
    Name = "web-server-sg"
  }
}

resource "aws_instance" "public-server" {
    instance_type = "t2.medium"
    ami = "ami-09e67e426f25ce0d7"
    key_name = "my-key"
    
    network_interface {
        device_index = 0
        network_interface_id = aws_network_interface.eni.id
    }
    
    user_data = <<-EOF
                  #!/bin/bash
                  apt -y update
                  apt -y install apache2
                  systemctl start apache2
                  bash -c 'echo your very first webserver by terraform > /var/www/html/index.html'
                  EOF
    
    tags = {
      "private-server" = "false"
      "Name" = "public-web-server"
    }
}

